package lt.vtvpmc.exam.ui.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SectorValidator implements Validator {

	private static final Logger log = LoggerFactory.getLogger(SectorValidator.class);

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		int sector = 0;

		if (value instanceof Integer) {
			sector = (Integer) value;
		} else {
			FacesMessage message = new FacesMessage("Not a number");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}

		if (sector < 1 || sector > 40) {
			FacesMessage message = new FacesMessage("Please write a number from 1 to 40");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}
}
