package lt.vtvpmc.exam.ui.controllers;

import java.io.Serializable;
import java.util.List;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.services.ReportsService;

public class ReportsController implements Serializable {

	private static final long serialVersionUID = 5989889322482017778L;

	private ReportsService reportsService;

	public List<Client> reportTop5ClientsByWeight() {
		return reportsService.findTop5ClientsByWeight();
	}

	public List<Client> reportTop5ClientsByUnits() {
		return reportsService.findTop5ClientsByUnits();
	}

	public void reportTop5SectorsByWeight() {

	}

	public void reportTop5SectorsByUnits() {

	}

	public ReportsService getReportsService() {
		return reportsService;
	}

	public void setReportsService(ReportsService reportsService) {
		this.reportsService = reportsService;
	}

}
