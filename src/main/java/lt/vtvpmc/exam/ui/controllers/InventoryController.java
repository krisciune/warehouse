package lt.vtvpmc.exam.ui.controllers;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Inventory;
import lt.vtvpmc.exam.services.InventoryService;
import lt.vtvpmc.exam.ui.models.ClientModel;
import lt.vtvpmc.exam.ui.models.InventoryModel;

public class InventoryController implements Serializable {

	private static final long serialVersionUID = -6091679281444163424L;

	private ClientModel clientModel;
	private InventoryModel inventoryModel;
	private InventoryService inventoryService;

	public void create() {
		inventoryModel.setCurrentInventory(new Inventory());
	}

	public void update(Inventory inventory) {
		inventoryModel.setCurrentInventory(inventory);
	}

	public String save() {
		inventoryModel.getCurrentInventory().setClient(clientModel.getCurrentClient());
		inventoryModel.getCurrentInventory().setEntryDate(new Date());
		inventoryService.save(inventoryModel.getCurrentInventory());
		inventoryModel.setCurrentInventory(new Inventory());
		return ClientController.NAV_LIST_INVENTORY;
	}

	public String delete(Inventory inventory) {
		inventoryService.delete(inventory);
		return ClientController.NAV_LIST_INVENTORY;
	}

	public String cancel() {
		inventoryModel.setCurrentInventory(new Inventory());
		return ClientController.NAV_LIST_INVENTORY;
	}

	public List<Inventory> findAllByCurrentClient() {
		if (clientModel.getCurrentClient() == null)
			return null;
		return inventoryService.findAllByClient(clientModel.getCurrentClient());
	}

	public Long countEntries(Client client) {
		return inventoryService.countEntriesByClient(client);
	}

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public InventoryModel getInventoryModel() {
		return inventoryModel;
	}

	public void setInventoryModel(InventoryModel inventoryModel) {
		this.inventoryModel = inventoryModel;
	}

	public InventoryService getInventoryService() {
		return inventoryService;
	}

	public void setInventoryService(InventoryService inventoryService) {
		this.inventoryService = inventoryService;
	}

}
