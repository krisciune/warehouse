package lt.vtvpmc.exam.ui.controllers;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.services.ClientService;
import lt.vtvpmc.exam.ui.models.ClientModel;

public class ClientController implements Serializable {

	private static final long serialVersionUID = 8760837696654598218L;

	static final Logger log = LoggerFactory.getLogger(ClientController.class);

	public static final String NAV_LIST_CLIENTS = "list-clients";
	public static final String NAV_LIST_INVENTORY = "list-inventory";

	private ClientModel clientModel;
	private ClientService clientService;

	public void create() {
		clientModel.setCurrentClient(new Client());
	}

	public String delete(Client client) {
		clientService.delete(client);
		return NAV_LIST_CLIENTS;
	}

	public List<Client> getClientList() {
		return clientService.findAll();
	}

	public String update(Client client) {
		clientModel.setCurrentClient(client);
		return NAV_LIST_INVENTORY;
	}

	public String save() {
		clientService.save(clientModel.getCurrentClient());
		return NAV_LIST_CLIENTS;
	}

	public String cancel() {
		clientModel.setCurrentClient(new Client());
		return NAV_LIST_CLIENTS;
	}

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

}
