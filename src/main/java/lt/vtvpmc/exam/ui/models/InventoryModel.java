package lt.vtvpmc.exam.ui.models;

import java.io.Serializable;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vtvpmc.exam.entities.Inventory;

public class InventoryModel implements Serializable {

	private static final long serialVersionUID = -4516264053477353885L;

	static final Logger log = LoggerFactory.getLogger(InventoryModel.class);

	@Valid
	private Inventory currentInventory;

	public void init() {
		currentInventory = new Inventory();
	}

	public Inventory getCurrentInventory() {
		return currentInventory;
	}

	public void setCurrentInventory(Inventory currentInventory) {
		this.currentInventory = currentInventory;
	}

}
