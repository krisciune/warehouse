package lt.vtvpmc.exam.ui.models;

import lt.vtvpmc.exam.entities.Client;

import java.io.Serializable;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientModel implements Serializable {

	private static final long serialVersionUID = 5040310448257955354L;

	static final Logger log = LoggerFactory.getLogger(ClientModel.class);

	@Valid
	private Client currentClient;

	public void init() {
		currentClient = new Client();
	}

	public Client getCurrentClient() {
		return currentClient;
	}

	public void setCurrentClient(Client currentClient) {
		this.currentClient = currentClient;
	}

}