package lt.vtvpmc.exam.entities.repositories;

import java.util.List;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Inventory;

public interface InventoryRepository {

	public void save(Inventory inventory);

	public void delete(Inventory inventory);

	public List<Inventory> findAll();

	public List<Inventory> findAllByClient(Client client);

	public Long countEntriesByClient(Client client);

}
