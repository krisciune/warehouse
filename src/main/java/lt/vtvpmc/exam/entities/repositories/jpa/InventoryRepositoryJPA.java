package lt.vtvpmc.exam.entities.repositories.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Inventory;
import lt.vtvpmc.exam.entities.repositories.InventoryRepository;

public class InventoryRepositoryJPA implements InventoryRepository {

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	@Override
	public void save(Inventory inventory) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(inventory) && inventory.getId() != null) {
				inventory = entityManager.merge(inventory);
			}
			entityManager.persist(inventory);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void delete(Inventory inventory) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			inventory = entityManager.merge(inventory);
			entityManager.remove(inventory);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Inventory> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Inventory> cq = cb.createQuery(Inventory.class);
			Root<Inventory> root = cq.from(Inventory.class);
			cq.select(root);
			TypedQuery<Inventory> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Inventory> findAllByClient(Client client) {
		EntityManager entityManager = getEntityManager();
		try {
			client = entityManager.merge(client);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Inventory> cq = cb.createQuery(Inventory.class);
			Root<Inventory> root = cq.from(Inventory.class);
			cq.select(root);
			cq.where(cb.equal(root.get("client"), client));
			TypedQuery<Inventory> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Long countEntriesByClient(Client client) {
		EntityManager entityManager = getEntityManager();
		try {
			client = entityManager.merge(client);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Inventory> root = cq.from(Inventory.class);
			cq.select(cb.count(root));
			cq.where(cb.equal(root.get("client"), client));
			TypedQuery<Long> q = entityManager.createQuery(cq);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

}
