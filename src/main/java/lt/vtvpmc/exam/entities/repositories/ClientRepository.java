package lt.vtvpmc.exam.entities.repositories;

import java.util.List;

import lt.vtvpmc.exam.entities.Client;

public interface ClientRepository {

	void save(Client newClient);

	void delete(Client client);

	List<Client> findAll();

	Client findById(Long id);

	Long countClients();

	List<Client> findTop5ByWeight();

	List<Client> findTop5ByUnits();

}
