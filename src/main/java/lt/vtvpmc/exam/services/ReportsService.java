package lt.vtvpmc.exam.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.repositories.ClientRepository;

public class ReportsService {

	static final Logger log = LoggerFactory.getLogger(ClientService.class);

	private ClientRepository clientRepo;

	public List<Client> findTop5ClientsByWeight() {
		return clientRepo.findTop5ByWeight();
	}

	public List<Client> findTop5ClientsByUnits() {
		return clientRepo.findTop5ByUnits();
	}

	public ClientRepository getClientRepo() {
		return clientRepo;
	}

	public void setClientRepo(ClientRepository clientRepo) {
		this.clientRepo = clientRepo;
	}

}
