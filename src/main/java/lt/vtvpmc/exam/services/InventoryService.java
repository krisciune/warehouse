package lt.vtvpmc.exam.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Inventory;
import lt.vtvpmc.exam.entities.repositories.InventoryRepository;

public class InventoryService {

	static final Logger log = LoggerFactory.getLogger(InventoryService.class);

	private InventoryRepository inventoryRepo;

	public void save(Inventory inventory) {
		inventoryRepo.save(inventory);
	}

	public void delete(Inventory inventory) {
		inventoryRepo.delete(inventory);
	}

	public List<Inventory> findAllByClient(Client client) {
		return inventoryRepo.findAllByClient(client);
	}

	public InventoryRepository getInventoryRepo() {
		return inventoryRepo;
	}

	public void setInventoryRepo(InventoryRepository inventoryRepo) {
		this.inventoryRepo = inventoryRepo;
	}

	public Long countEntriesByClient(Client client) {
		return inventoryRepo.countEntriesByClient(client);
	}
}
