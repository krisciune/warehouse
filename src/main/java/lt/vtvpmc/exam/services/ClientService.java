package lt.vtvpmc.exam.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.repositories.ClientRepository;

public class ClientService {

	static final Logger log = LoggerFactory.getLogger(ClientService.class);

	private ClientRepository clientRepo;

	public void delete(Client client) {
		clientRepo.delete(client);
	}

	public List<Client> findAll() {
		return clientRepo.findAll();
	}

	public Client findById(Long currentClientId) {
		return clientRepo.findById(currentClientId);
	}

	public void save(Client currentClient) {
		clientRepo.save(currentClient);
	}

	public Long countClients() {
		return clientRepo.countClients();
	}

	public ClientRepository getClientRepo() {
		return clientRepo;
	}

	public void setClientRepo(ClientRepository clientRepo) {
		this.clientRepo = clientRepo;
	}

}
